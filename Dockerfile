FROM node:14

WORKDIR /app

ADD ./source/ /app/

EXPOSE 4200

CMD npm run start -- --host 0.0.0.0
